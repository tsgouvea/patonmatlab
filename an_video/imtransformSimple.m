function imageOut = imtransformSimple(imageIn, transform)
% imageOut = imtransformSimple(imageIn, transform)
%
% Performs a rigid transform, cropping the resulting image to the
% dimensions of the input image.
%
% Takes in an image and a 3-long rigid body transform vector containing
% [xTranslation yTranslation rotationAngle]
%
% Returns resulting image, cropped to the dimensions of imageIn, with
% unknown regions 0.

imageOut = imtransform(imageIn, getTransform(imageIn, transform), ...
    'XData', [1 size(imageIn, 2)], 'YData', [1 size(imageIn, 1)]);