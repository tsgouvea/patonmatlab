%% vid2predict.m
% Takes a fullpath to a video file as input, runs all the analyses (i.e. 
% extracts relevant frames, runs PCA across them, then classifies with LDA)

%% Preamble
clear all
close all
clc
if ismac
    [~,username] = system('whoami');
    if strcmp(username(1:end-1),'thiago')
        dbDir = '/Users/thiago/Dropbox/Paton Lab/';
    elseif strcmp(username(1:end-1),'Tiago')
        dbDir = '/Users/Tiago/Dropbox/PatonLab/PatonLab/';
    elseif strcmp(username(1:end-1),'ptiagomonteiro')
        dbDir = '/Users/ptiagomonteiro/Dropbox/PatonLab/PatonLab/';
    elseif strcmp(username(1:end-1),'jp2063')
        dbDir = '/Users/jp2063/Dropbox/PatonLab/';
    end
    
elseif isunix
    dbDir = '/home/thiago/Dropbox/Paton Lab/';
end
dataDir = [dbDir 'Data/'];

%% Parameters
session = '131201';
subjName = 'Edgar';
ndxLed = findtrial(rawLed);
rang = [-.5 2.5];
step = .1;
fps = 120;
F = round((rang(1):step:rang(2))*fps);

%% Loading variables
fileSess = dir([dataDir 'TAFC/Behavior/' subjName '/*' session '*.*']);
dataParsed = parsedata([dataDir 'TAFC/Behavior/' subjName '/' fileSess.name]);
fileLed = dir([dataDir 'TAFC/Video/' subjName '/' session '/*led.csv']);
rawLed = csvread([dataDir 'TAFC/Video/' subjName '/' session '/' fileLed.name]);
% fileTs = dir([dataDir 'TAFC/Video/' subjName '/' session '/*ts.csv']);
% rawTs = csvread([dataDir 'TAFC/Video/' subjName '/' session '/' fileTs.name]);
fileVid = dir([dataDir 'TAFC/Video/' subjName '/' session '/*avi']);
%% Extract frames


system(['avconv ' dataDir 'TAFC/Video/' subjName '/' session '/' fileVid.name ])