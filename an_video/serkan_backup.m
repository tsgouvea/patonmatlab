% Saves a number of snapshots before, during and after each trial initiation

% Serkan

% FFMPEG line that was used to create the videos is below

% ffmpeg �i Bertrand_TAFCv02_120904_TG.webm �vcodec libxvid �b:v 800k �maxrate 800k �bufsize 1600k �r 14.6 �vsync 1 �an �t 01:08:00 Bertrand_TAFCv02_120904_TG.avi
% avconv -i /media/Thiago\ 1TB/Video/Bertrand/Bertrand_TAFCv02_120901_TG.webm -vcodec libxvid -b:v 800k -maxrate 800k -bufsize 1600k -r 14.6 -vsync 1 -an -t 01:40:00 /home/thiago/Desktop/Bertrand_TAFCv02_120901_TG.avi


close all
clear all
clc

%% Inputs

[videoName,videoPath,~] = uigetfile('.avi','Select video file');
fileNameWithoutExtension = videoName(1:end-4);
vid = VideoReader([videoPath videoName]);
fps = vid.FrameRate;
nFrames = vid.NumberOfFrames;

parsed = parseBHV;
syncMilis = parsed.SyncPulse; %Array of miliseconds that sync led is on
initMilis = parsed.TrialInit; %Array of miliseconds of trial initiations

yesorno = input('Is the file for red levels present? (y/n): ', 's');

if yesorno == 'y'    
    [fileName,filePath,~] = uigetfile('.mat','Select red levels file');
    redLevels = load([filePath fileName]);
    redLevels = redLevels.red_levels;
end

first = input('Enter start time for snapshots in seconds (e.g. -1): ');
last = input('Enter end time for snapshots in seconds (e.g. 3): ');
inc = input('Enter increment for snapshots in seconds (e.g. 0.2): ');

%% Optional partial processing

partialProcessing = 0;

if(partialProcessing)
    fStart=1; %User defined frame range for partial processing
    fEnd=nFrames;
else
    fStart=1;
    fEnd=nFrames;
end
    
if yesorno ~= 'y'
    %% Define LED region

    testFrame = read(vid,1);
    imshow(testFrame);
    title('Use the mouse to mark the tip of the LED');
    rect = getrect;
    rect = round(rect);
    x = rect(1):(rect(1)+rect(3));
    y = rect(2):(rect(2)+rect(4));
    close;

    %% Correction

    % While cropping, some Matlab versions take the y coordinates first, some
    % take them second. This part compares the mean red levels between
    % two images which the x and y coordinates are swapped and makes the
    % correction if necessary.

    cropped1 = testFrame(y,x,1);
    cropped2 = testFrame(x,y,1);

    if mean(cropped1(:)) < mean(cropped2(:))
        x_temp = x;
        x = y;
        y = x_temp;
    end

    %% Get red levels
    tic
    redLevels=zeros(1,nFrames);

    for i = fStart : fEnd               
        if(rem(i,500)==0) %To see the progress
            fEnd - i
        end
        try %In case of read errors
            frame = read(vid,i);
        catch
            display('Unable to read');
        end
        cropped = frame(y,x,1); %Red channel only
        redLevels(i) = mean(cropped(:));
    end

    if partialProcessing==0
        save(['Red levels/RED_' fileNameWithoutExtension '.mat'], 'redLevels');
    end

    toc
end
%% Thresholding

% A blink is identified if a one frame's average red level is much higher
% (difference is larger than the threshold) than two frames before and two
% frames after. (Because sometimes a blink is captured by two frames)

% While each frame is checked, a range of 5 frames is used. For speed, 
% these 5 frames aren't evaluated over and over, they are all evaluated at first,
% and then only the 6th frame is evaluated and then they are shifted.

threshold= (max(redLevels) - mean(redLevels)) / 5;
n=length(redLevels);
syncFrames=nan(1,length(syncMilis)); %Array of frame numbers of sync LED on
pos=1;
for k=1:(n-2)
    if (k==1)
        prepre = redLevels(1);    
        pre = redLevels(2);
        cur = redLevels(3);
        next = redLevels(4);
        nextnext = redLevels(5);
    end

    if(k~=1)
        nextnext = redLevels(k+2);
    end

    if((cur - prepre > threshold) && (cur - nextnext > threshold))
        if(pos==1)
            syncFrames(pos)=k;
            pos=pos+1;
        else
            if((syncFrames(pos-1)~=(k-1))) % if two frames capture a blink, disregard the second one
                syncFrames(pos)=k;
                pos=pos+1;
            end
        end
    end
    
    prepre=pre;
    pre=cur;    
    cur=next;
    next=nextnext;    
end
syncFrames(isnan(syncFrames))=[];
% Check if the last blink belongs to a valid trial
if isnan(initMilis(end))
    initMilis(end) = [];
    syncMilis(end) = [];
end

if partialProcessing == 0
    save(['LED ON/LED_ON_' fileNameWithoutExtension '.mat'], 'syncFrames');
end

%% Delete captured trials which are missing in the behavioral file

apprxSyncFrames = syncFrames(1) + round( syncMilis / 1000 * fps );
newSyncFrames = syncFrames;
notInBehavior = nan(1,length(newSyncFrames));
pos=1;
i=1;
endOfArray = 0; % Boolean

while ~endOfArray
    
    frameFound = 0; %boolean (frame number from the behavior file is also found by the video)
    k = 1;
    
    while ~frameFound && k<=length(apprxSyncFrames)        
        frameFound = abs( newSyncFrames(i) - apprxSyncFrames(k) ) < 10;
        k = k+1;        
    end
        
    if ~frameFound       
        notInBehavior(pos) = newSyncFrames(i); % Stores the indice, to check afterwards
        pos = pos + 1;
        newSyncFrames(i)=[];
    else
        i=i+1;
    end
    
    if i>length(newSyncFrames)
        endOfArray = 1;
    end
        
end

notInBehavior(isnan(notInBehavior))=[];

%% Recover non-captured trials

nonCapturedFrames = nan(1,length(apprxSyncFrames));
nonCapturedIndices = nan(1,length(apprxSyncFrames));
pos = 1;

for i=1:length(apprxSyncFrames)
    
    frameFound = 0; %boolean (frame number from the behavior file is also found by the video)
    k = 1;
    
    while ~frameFound && k<=length(newSyncFrames)        
        frameFound = abs( apprxSyncFrames(i) - newSyncFrames(k) ) < 10;
        k = k+1;        
    end
        
    if ~frameFound
        insertFrame = newSyncFrames(i-1) + round(fps / 1000 * ( syncMilis(i) - syncMilis(i-1) ));
        newSyncFrames = [newSyncFrames(1:(i-1)) insertFrame newSyncFrames(i:end)];
        nonCapturedFrames(pos) = insertFrame;
        nonCapturedIndices(pos) = i;
        pos = pos + 1;
    end
        
end

nonCapturedFrames(isnan(nonCapturedFrames))=[];
nonCapturedIndices(isnan(nonCapturedIndices))=[];

%% To take snapshots, convert time intervals to frame intervals 

firstFrame = round(first * fps);
lastFrame = round(last * fps);
incFrame = round(inc * fps);

% Make sure to capture trial initiation frame
firstFrame = incFrame * floor(firstFrame/incFrame);

% Make sure to define a last frame
lastFrame = incFrame * ceil(lastFrame/incFrame);

display(['New first and last are ' num2str(firstFrame*fps) ' and ' num2str(lastFrame*fps) ' seconds'])

%% Create and fill array for frames of trial initiation

initFrames = nan(1,length(newSyncFrames));

for i = 1:length(newSyncFrames)
   
    initFrames(i) = newSyncFrames(i) + round( ( initMilis(i) - syncMilis(i) ) / 1000 * fps );
   
end

%% Take snapshots

for i = 1:length(initFrames) %For each trial
       
       readError = 0;
       folderName = ['Snapshots/' fileNameWithoutExtension '/' num2str(i) '/'];
       mkdir(folderName);
       num = 1;
       for j = firstFrame : incFrame : lastFrame

           frameNumber = initFrames(i) + j;
           try
               frame = read(vid,frameNumber);
           catch
               display('Cannot read frame');
               readError = 1; % Boolean
           end
           
           if ~readError
               timestamp = round(1000*j/fps);
               fileNameAndFolder = [folderName num2str(i) '_' num2str(num) '_' num2str(timestamp) '.jpg'];
               imwrite(frame, fileNameAndFolder, 'jpg');
           end
           num = num + 1;

       end
    
end

