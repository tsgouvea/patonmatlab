function [ syncV ] = findtrial( rawLed, comb02, hipass )
%FINDTRIAL Detects trial onsets from sync led
%   Detects trial availability time from analog, temporaly continuous
%   led luminance
narginchk(2,3)
nLed = zscore(rawLed);
if nargin < 3
    thLed = double(nLed>8);
    syncV = find([false; diff(thLed)==1]);
elseif hipass
    N = 1;
    fs = 120;
    cutoff_frequency = 20; % Hz
    Wn = (cutoff_frequency/(fs/2));
    [B,A] = butter(N,Wn,'high');
    Y = filter(B,A,nLed);
    nLed = zscore(Y);
    thLed = double(nLed>15);
    syncV = find([false; diff(thLed)==1]);
    
    %%
%     figure, hold on
%     t = (1:length(rawLed))/fs;
%     plot(t,zscore(rawLed))
%     plot(t,nLed,'r')
end
%% Aligning trials
syncB = comb02.bhv.SyncPulse(:);
syncV = syncV(:);

if numel(syncV)>numel(syncB)
    dif = numel(syncV)-numel(syncB);
    df = dif+1; % degrees of freedom
    for d = 1:df
        new_syncV = syncV(d:end-(df-d));
        if numel(new_syncV)==numel(syncB) && corr(diff(new_syncV),diff(syncB)) > 0.99
            syncV = new_syncV;
            break
        end
    end
elseif numel(syncV)<numel(syncB)
    dif = numel(syncB)-numel(syncV);
    df = dif+1;
    for d = 1:df
        %new_syncB = syncB(d:end-(df-d));
        new_syncV = nan(size(syncB));
        new_syncV(d:end-(df-d)) = syncV;
        nanNdx = isnan(new_syncV);
        if numel(new_syncV)==numel(syncB) && corr(diff(new_syncV(~nanNdx)),diff(syncB(~nanNdx))) > 0.99
            syncV = new_syncV;
            break
        end
    end
elseif corr(diff(syncV),diff(syncB)) < .99
    mismatch = true;
    D = 0;
    while mismatch
%         display(['Iter ' sprintf('%3d',D)])
        D = D+1;
        
        new_syncV = nan(size(syncV));
        new_syncV(1:end-D) = syncV(1+D:end);
        nanNdx = isnan(new_syncV);
        if numel(new_syncV)==numel(syncB) && corr(diff(new_syncV(~nanNdx)),diff(syncB(~nanNdx))) > 0.99
            syncV = new_syncV;
            mismatch = false;
            break
        end
        
        new_syncV = nan(size(syncV));
        new_syncV(1+D:end) = syncV(1:end-D);
        nanNdx = isnan(new_syncV);
        if numel(new_syncV)==numel(syncB) && corr(diff(new_syncV(~nanNdx)),diff(syncB(~nanNdx))) > 0.99
            syncV = new_syncV;
            mismatch = false;
            break
        end
        
        if D > numel(syncB)
            error('PatonLab:neur','Failed to align trials. Are you sure both files refer to the same session?')
        end
    end
end

nanNdx = isnan(syncV);
if numel(syncV)~=numel(syncB) || corr(diff(syncV(~nanNdx)),diff(syncB(~nanNdx))) < .99
    error('PatonLab:neur','Trials misaligned.')
end

%% JUNK
% 
% 
% %%
% % N is the order of the filter - if you are unsure what this is, just set it to 10.
% % Wn is the cutoff frequency normalized between 0 and 1, with 1 corresponding to half the sample rate of the signal. If your sample rate is fs, and you want a .
% N = 10;
% fs = 120;
% cutoff_frequency = 10; % Hz
% Wn = (cutoff_frequency/(fs/2));
% [B,A] = butter(N,Wn,'high');
% % You can then apply the filter by using
% X = rawLed;
% Y = filter(B,A,X);
% t = (1:length(X))/fs;
% % figure
% clf
% plot(t,X)
% hold on, plot(t,zscore(Y),'r')
% % where X is your signal.
% % You can also look into the filtfilt function.

end

