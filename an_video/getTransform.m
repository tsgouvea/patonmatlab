function xform = getTransform(image, transform)
% xform = getTransform(image, transform)
%
% Takes as input a 2d image, and a 3 long vector defining a rigid transform
% that has [xTranslation yTranslation rotationAngle], with rotation around
% the center of the image. 
%
% Returns a tform object that can be passed to imtransform.


xTrans = transform(1);
yTrans = transform(2);
angle = transform(3);
%point around which to rotate
x = size(image, 2)/2;
y = size(image, 1)/2;

xform = maketform('affine', [cos(angle), -sin(angle), 0; ...
                            sin(angle), cos(angle), 0; ...
                            xTrans + x - x*cos(angle) - y*sin(angle), -yTrans + y + x*sin(angle) - y * cos(angle), 1]);