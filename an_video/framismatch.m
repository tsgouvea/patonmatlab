function [ minusrho ] = framismatch( template, imageIn, transform, mask )
%FRAMISMATCH Correlation coefficient between a template and a transformed
%image. transf is a 3-element vector containing x and y translation and
%angle of rotaion around the center

% obj = getTransform(img, transf);
if nargin == 3
    mask = true(size(template));
end
imageOut = imtransformSimple(imageIn, transform);
minusrho = corr(template(mask), imageOut(mask))*-1;

end

