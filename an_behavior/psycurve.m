function [psyc, stimSet, betas, slope, bias, asymptotes] = psycurve(varargin)
%PSYCURVE Psychometric curve
%
%   [psyc, stimSet] = PSYCURVE(stimuli,choices) calculates the proportion
%   of positive choices (psyc) for each unique stimulus value (stimSet).
%
%   [psyc, stimSet] = PSYCURVE(dataParsed) reads stimuli and choices from
%   dataParsed.Interval and dataParsed.ChoiceLeft, respectively.
%
%   [psyc, stimSet, betas] = PSYCURVE(...) additionally fits the data with 
%   a 4 parameter sigmoid of the following form:
%
%       Y = betas(1) + betas(2)./(1+exp(-1*betas(3)*(X-betas(4))))
%
%   Additional outputs:
%   [psyc, stimSet, betas, slope, bias, asymptotes] = PSYCURVE(...)
%   slope is calculated at half-height (i.e. point of subjective equality),
%   and equals (betas(2)*betas(3))/4.
%   bias is betas(4)
%   asymptotes are [betas(1) and betas(1)+betas(2)]


narginchk(1,2)

if nargin == 1
    dataBhv = varargin{1};
    intervals = dataBhv.Interval(:);
    if isfield(dataBhv,'ChoiceLong')
        choices = dataBhv.ChoiceLong(:);
    else
        choices = dataBhv.ChoiceLeft(:);
    end
        
else
    intervals = varargin{1}(:);
    choices = varargin{2}(:);
end

nanNdx = isnan(intervals) | isnan(choices);
intervals = intervals(~nanNdx);
choices = choices(~nanNdx);
stimSet = unique(intervals); stimSet = stimSet(:);

if nargout > 2
    fitPsy = true;
else
    fitPsy = false;
end

choices(choices==-1) = 0;
psyc = nan(size(stimSet));

for iInt = 1:length(stimSet)
    iNdx = intervals==stimSet(iInt);
    psyc(iInt) = sum(choices(iNdx))/sum(iNdx);
end

if fitPsy
%     try
%         [betas] = fminsearchbnd(@(param) sig4sse(param,stimSet,psyc), [min(psyc) max(psyc)-min(psyc) mean(stimSet) 20],[0 0 0 -Inf],[1 1 1 Inf]);
%     catch
%         warning('Psychofit: using unbound optimization')
        [betas] = fminsearch(@(param) sig4sse(param,stimSet,psyc), [min(psyc) max(psyc)-min(psyc) mean(stimSet) 1]);
%     end
    if nargout > 3
        slope = prod(betas([2 3]))/4;
        if nargout > 4
            bias = betas(4);
            if nargout > 5
                asymptotes = [betas(1) betas(1)+betas(2)];
            end
        end
    end
end

    function [ error ] = sig4sse(betas, X, Y )
        Yhat = betas(1) + betas(2)./(1+exp(-1*betas(3)*(X-betas(4))));
        error = nansum((Y-Yhat).^2);
    end
end