function [ varargout ] = daily_report( bhv, hFig )
%daily_report Generates a session report figure
% input argument = output of parsedata function;
%%
narginchk(1,2)

if ~isstruct(bhv)
    bhv = parseBHV(bhv);
end    
if nargin < 2
    hFig = figure('windowstyle','docked');
else
    figure(hFig)
    clf
end

%%
if strcmp(bhv.Protocol,'TAFC')
    myxtick = [0 .33 .5 .67 1];
    myxticklabel = myxtick * bhv.Scaling / 1000;
    myxticklabel([1 2 4 5]) = round(myxticklabel([1 2 4 5]));
    
    %% Plot formatting and init
    % set(0,'DefaultLineLineWidth',3)
    % set(0,'DefaultAxesColorOrder',[0.0078    0.6784    0.9216; 0.9569    0.1059    0.1373])
    % set(0,'DefaultAxesFontName','Arial')
    % set(0,'DefaultAxesFontSize',14)
    % set(0,'DefaultAxesBox','off')
    % set(0,'DefaultLineMarkerSize',20)
    
    %% Blocks
    myalpha = .1;
    subplot(3,3,[1 4 7]), hold on
    transitions = find(diff(bhv.BlockNumber)==1);
    if ~isempty(transitions)
        if bhv.LeftLong(transitions(1))
            color = 'c';
        else
            color = 'm';
        end
        patch([0 1 1 0],[0 0 transitions(1) transitions(1)],color,'edgecolor','none','facealpha',myalpha)
        if strcmp(color,'c')
            color = 'm';
        elseif strcmp(color,'m')
            color = 'c';
        end
        patch([0 1 1 0],[transitions(1) transitions(1) [1 1]*length(bhv.ChoiceCorrect)],color,'edgecolor','none','facealpha',myalpha)
    else
        if bhv.LeftLong(1)
            color = 'c';
        else
            color = 'm';
        end
        patch([0 1 1 0],[0 0 1 1]*length(bhv.ChoiceCorrect),color,'edgecolor','none','facealpha',myalpha)
    end
    %% Trial history - Valid trials
    valid_ndx = ~isnan(bhv.ChoiceCorrect);
    correct_ndx = find(bhv.ChoiceCorrect==1);
    incorrect_ndx = find(bhv.ChoiceCorrect==0);
    plot(bhv.Interval(correct_ndx),correct_ndx,'*g','markersize',3)
    plot(bhv.Interval(incorrect_ndx),incorrect_ndx,'*r','markersize',3)
    
    %% Trial history - Other trials
    
    if ~isempty(strfind(bhv.ProtocolVersion,'Fix'))
        broke_ndx = find(bhv.BrokeFix);
        plot(bhv.BrokeFixTime(broke_ndx)/bhv.Scaling,broke_ndx,'*','color',[.5 .5 .5],'markersize',3);
    else
        prem_long_ndx = find(bhv.PrematureLong==1);
        prem_short_ndx = find(bhv.PrematureLong==0);
        prem_ndx = bhv.Premature==1;
        plot(bhv.PremTime(prem_long_ndx)/bhv.Scaling,prem_long_ndx,'*','color',[.5 .5 .5],'markersize',3);
        plot(bhv.PremTime(prem_short_ndx)/bhv.Scaling,prem_short_ndx,'*k','markersize',3);
    end
    % rw_miss_ndx = find(dataParsed.RwdMiss==1);
    % choice_miss_ndx = find(dataParsed.ChoiceMiss==1);
    %
    %
    % plot(dataParsed.Interval(rw_miss_ndx),rw_miss_ndx,'*','color',[.8 .8 .3],'markersize',3)
    % plot(dataParsed.Interval(choice_miss_ndx),choice_miss_ndx,'*','color',[.3 .8 .8],'markersize',3)
    
    %% Trial history - Formatting plot
    axis([0 1 [-.01 1.01]*length(bhv.TrialNumber)])
    ax(1) = gca;
    set(ax(1),'box','off','tickdir','out','YDir','reverse','xtick',myxtick,'xticklabel',myxticklabel)
    ylabel Trial; xlabel 'Interval duration (s)'
    %axis tight
    
    %% Running average of performance
    x = bhv.Interval;
    % x = x(~isnan(x));
    % kernel = normpdf(linspace(-20,20,41),0,100);% (X,MU,SIGMA)
    windowSize = 20;
    kernel = ones(1,windowSize)/windowSize;    % kernel(1:20) = 0; kernel = kernel./(sum(kernel));
    % figure('windowstyle','docked'); plot(linspace(-20,20,41),kernel); axis tight
    
    avgValid = filter(kernel,1,bhv.ChoiceCorrect(valid_ndx));
    avgTotal = filter(kernel,1,bhv.ChoiceCorrect==1);
    % avgValid(1:20) = nan;
    % avgTotal(1:20) = nan;
    
    ax(2) = axes('Position',get(ax(1),'Position'),...
        'XAxisLocation','top',...
        'ytick',[],...
        'xtick',[0 .5 1],...
        'color','none',...
        'XColor',[0.0078 0.6784 0.9216]);
    xlabel 'Average performance'
    hold on
    %%mance'
    line(avgTotal,[1:length(avgTotal)],'LineWidth',3,'color',[.8 .8 .8],'Parent',ax(1));
    line(avgValid,find(valid_ndx),'LineWidth',3,'Parent',ax(1));
    line([0.5 0.5],[0 length(avgTotal)],'linestyle',':','LineWidth',1,'Parent',ax(1));
    % % x = x(~isnan(x));
    % x(isnan(x)) = 0.5;
    % n=round(length(dataParsed)/10);
    % kernel = normpdf(linspace(-1,1,n),0,1);
    % temp1 = x(1:n);
    % temp2 = x(end-n:end);
    % temp3 = [temp1; x; temp2];
    % conv_data = conv(temp3,kernel/sum(kernel),'same');
    % plot(conv_data(n+1:end-n),'LineWidth',1)
    
    
    %% Psychometric curve
    [psyc, IntervalSet] = psycurve(bhv);
    beta = glmfit(bhv.Interval(:),bhv.ChoiceLong(:),'binomial');
    
    xaxis = linspace(0,1,100);
    ycont = 1./(1+exp(-1*(beta(1)+xaxis*beta(2))));
    
    subplot(3,3,2), patch([xaxis fliplr(xaxis)],[ycont fliplr(ycont)],'b','edgecolor', [0.9569    0.1059    0.1373], 'edgealpha',.25,'linewidth',2), hold on
    plot(IntervalSet, psyc,'.','markersize',20)
    axis([0 1 0 1]);
    set(gca,'box','off','xtick',myxtick,'xticklabel',myxticklabel,'tickdir','out', 'color',[1 1 1])
    ylabel 'P(long choice)'
    
    %% Reaction Times
    [rt, rtsem, stimSet] = reactime(bhv,false);
    subplot(3,3,3), hold on
    
    patch([stimSet, fliplr(stimSet)],[rt(1,:)-rtsem(1,:), fliplr(rt(1,:)+rtsem(1,:))],[1 172 235]/255,'edgecolor','none','facealpha',0.2)
    patch([stimSet, fliplr(stimSet)],[rt(2,:)-rtsem(2,:), fliplr(rt(2,:)+rtsem(2,:))],'g','edgecolor','none','facealpha',0.2)
    patch([stimSet, fliplr(stimSet)],[rt(3,:)-rtsem(3,:), fliplr(rt(3,:)+rtsem(3,:))],'r','edgecolor','none','facealpha',0.2)
    
    plot(stimSet,rt(1,:),'color',[1 172 235]/255)
    plot(stimSet,rt(2,:),'g')
    plot(stimSet,rt(3,:),'r')
    
    set(gca,'box','off','tickdir','out','xtick',myxtick,'xticklabel',myxticklabel,'ytick',round(linspace(min(min(rt))-200, max(max(rt))+200, 3)), 'color',[1 1 1])
    axis([0 1 min(min(rt))-200, max(max(rt))+200])
    xlabel 'Interval duration (s)'; ylabel 'Reaction time (ms)'
    
    %% Trial onsets distribution
    subplot(3,3,5), hold on
    cum_ton = bhv.TrialAvail/60000;
    plot(cum_ton,[1:length(cum_ton)],'linewidth',2);
    plot(cum_ton(incorrect_ndx),incorrect_ndx,'r+','MarkerSize',2);
    plot(cum_ton(correct_ndx),correct_ndx,'g+','MarkerSize',2);
    % axis([0 max(cum_ton) 1 length(dataParsed)])
    set(gca,'box','off','tickdir','out')
    ylabel '# of trials'; xlabel 'Time (min)'
    axis tight
    legend({'All' 'Correct' 'Errors'},'location','southeast');
    legend boxoff
    margins;
    
    %% Cumulative distributions of choice
    subplot(3,3,6), hold on
    plot(1:length(bhv.ChoiceLeft), cumsum(bhv.ChoiceLeft==1), 'c','linewidth',2)
    plot(1:length(bhv.ChoiceLeft), cumsum(bhv.ChoiceLeft==0), 'm','linewidth',2)
    xlabel('Trial number', 'FontSize', 10, 'FontWeight', 'bold' )
    ylabel('Choice number', 'FontSize', 10, 'FontWeight', 'bold' )
    hold off
    leg = legend('Left', 'Right', 'Location', 'Best');
    set(leg, 'Box', 'off', 'Fontsize', 8)
    margins;
    
    if ~isempty(strfind(bhv.ProtocolVersion,'Fix'))
        subplot(3,3,9), hold on
%         edges = linspace(0,max(max(dataParsed.FixTime),max(dataParsed.BrokeFixTime)),20);
        edges = [0:100:max(max(bhv.IntervalPrecise),max(bhv.BrokeFixTime)),Inf];
        timeb = histc(bhv.BrokeFixTime, edges);
        timec = histc(bhv.IntervalPrecise, edges);
        barb = bar(edges(1:end-1),timeb(1:end-1),'r');
        set(get(barb,'Children'),'facealpha',0.5,'edgealpha',0);
        hold on
        barc = bar(edges(1:end-1),timec(1:end-1),'g');
        set(get(barc,'Children'),'facealpha',0.5,'edgealpha',0);
        xlim([-100 3000])
        xlabel 'Fixation time (ms)'
        ylabel Counts
%         legend({'Broken', 'Completed'},'location','northeast','box','off')
%         legend boxoff
%        margins;
    end
    
    %% Gallistel's
    subplot(3,3,8)
    x = bhv.ChoiceCorrect(~isnan(bhv.ChoiceCorrect));
    x(x==0) = -1;
    plot(cumsum(x),'linewidth',2);
    xlabel({'Trial #';'(valid only)'})
    ylabel 'Cumulative Corrects'
    margins;
    set(gca,'box','off','tickdir','out')
    
    %%
    
    
    %%
    annotation('textbox',[.60 .5 .5 .5],'edgecolor','none','String',[bhv.Animal ' ' bhv.Date],'fontsize',20,'fontname','Arial','interpreter','none');
    
    %%
    str = ['Performance \nOverall:\t' sprintf('%2.1f',nansum(bhv.ChoiceCorrect)/length(bhv.ChoiceCorrect)*100)...
        '%%\nValid only:\t' sprintf('%2.1f',nanmean(bhv.ChoiceCorrect)*100) '%%\n\n'];
    fprintf(str)
    varargout{1} = nanmean(avgValid);
    varargout{2} = hFig;
elseif strcmp(bhv.Protocol,'MATCHING')
    %% Reward and choice computations
    
    valid = find(~isnan(bhv.Rewarded));
    invalid = find(isnan(bhv.Rewarded));
    
    rewNdx = find(bhv.Rewarded == 1);
    unrewNdx = find(bhv.Rewarded == 0);
    
    choice_left = find(bhv.ChoiceLeft == 1);
    choice_right = find(bhv.ChoiceLeft == 0);
    
    %% Plots of animal's choice and probability of reward
    
    subplot(3,4,[1, 4]), hold on
    axis([1, length(bhv.ChoiceLeft), 0, 1])
    plot(rewNdx,bhv.ChoiceLeft(rewNdx),'og','markersize',3)
    plot(unrewNdx,bhv.ChoiceLeft(unrewNdx),'or','markersize',3)
    plot(invalid, 0.5,'ok','markersize',3)
    ax(1) = gca;
    set(ax(1),'box','off','tickdir','out','ytick',[0, 0.5, 1],'yticklabel',{'Right','Miss', 'Left'}); % 'FontSize', 11, 'FontWeight', 'bold' )
    xlabel ('Trial number', 'FontSize', 10, 'FontWeight', 'bold' );
    ylabel ('Choice', 'FontSize', 10, 'FontWeight', 'bold' )
    % str2 = ['Prob:   ', num2str(unique(data.ProbRwdLeft/100)), ',   stim dur:   ', num2str(unique(data.LaserDur)), ' (ms)', ',  stim freq:   ', num2str(unique(data.LaserFreq)), ' (Hz)'];
    % if length(str2) > length(str1)
    %     d = length(str2) - length(str1);
    %     if d > 0
    %         str1 = [str1, zeros(d)];
    %     else
    %         str2 = [str2, zeros(d)];
    %     end
    % end
    annotation('textbox',[.60 .5 .5 .5],'edgecolor','none','String',[bhv.Animal ' ' bhv.Date],'fontsize',20,'fontname','Arial','interpreter','none');

    %% Running average of choice
    windowSize = 10;
    kernel = ones(1,windowSize)/windowSize;
    avg_left = filter(kernel,1,bhv.ChoiceLeft(valid));
    line(valid, avg_left,'LineWidth',1,'color','k','Parent',ax(1));
    
    %% Running average of reward probability
    p_rwd_left = bhv.ProbRwdLeft/100;
    p_rwd_right = bhv.ProbRwdRight/100;
    line([1:length(p_rwd_left)], p_rwd_left./(p_rwd_left+p_rwd_right),'LineStyle',':', 'LineWidth',1,'color','b','Parent',ax(1))
%     line([1:length(p_rwd_left)], p_rwd_left/sum(unique([unique(p_rwd_left) unique(p_rwd_right)])),'LineStyle',':', 'LineWidth',1,'color','b','Parent',ax(1))
    
    ax(2) = axes('Position',get(ax(1),'Position'),'YAxisLocation','right', 'tickdir','out','xtick',[],'ytick', 0:0.2:1,'color','none','YColor','b');
    ylabel ('Probability fraction(left)', 'FontSize', 10, 'FontWeight', 'bold')
    hold off
    
    %% Trial number as a function of time
    subplot(3,3,4), hold on
    trial_init = bhv.TrialInit(1:end)/60000;
    plot(trial_init(unrewNdx), unrewNdx,'or', 'MarkerSize', 3);
    plot(trial_init(rewNdx), rewNdx,'og', 'MarkerSize', 3);
    plot(trial_init(invalid), invalid,'ok', 'MarkerSize', 3);
    set(gca,'box','off','tickdir','out')
    ylabel('Trial number', 'FontSize', 10, 'FontWeight', 'bold' )
    xlabel('Time (min)', 'FontSize', 10, 'FontWeight', 'bold' )
    axis tight
    
    %% Counts of rewards, no-rewards and missed rewards
    subplot(3,3,5)
    n_rewarded = length(rewNdx);
    n_unrewarded = length(unrewNdx);
    n_rwd_miss = length(invalid);
    n_trials = n_rewarded + n_unrewarded + n_rwd_miss;
    rewarded_perc = 100 * n_rewarded/n_trials;
    y = [n_rwd_miss, n_rewarded, n_unrewarded; 0, 0, 0]/ (n_trials);
    bar( y, 'stack');
    set(gca,'xtick',[], 'xlim', [0.4, 4], 'box','off','tickdir','out')
    
    ylabel('Reward fraction', 'FontSize', 10, 'FontWeight', 'bold' )
    text(1.7,0.9,['N. of trials = ', num2str(n_trials)], 'FontSize', 8)
    text(1.7,0.7,['N. of rewards = ', num2str(n_rewarded)], 'FontSize', 8)
    text(1.7,0.5,['N. of Rew.Misses = ', num2str(n_rwd_miss)], 'FontSize', 8)
    text(1.7,0.3,['Rewarded = ', num2str(rewarded_perc,2), '%'], 'FontSize', 8)
    
    
    %% Initiation time distribution
    
    it = ( bhv.TrialInit(1:length(bhv.TrialAvail(1:end-1))) - bhv.TrialAvail(1:end-1) )/1000;
    subplot(3,3,6)
    n_bins = 500;
    hist(it, n_bins)
    xlabel ('Initiation time (s)', 'FontSize', 10, 'FontWeight', 'bold' )
    ylabel ('Counts', 'FontSize', 10, 'FontWeight', 'bold' )
    set(gca,'box','off','tickdir','out')
    axis tight
    it_prctile = prctile(it,[0, 95]);
    xlim(it_prctile)
    
    %% Reaction time distribution
    c = bhv.ReactionTime/1000;
    subplot(3,3,7)
    n_bins = 100;
    hist(c, n_bins)
    xlabel ('Reaction time (s)', 'FontSize', 10, 'FontWeight', 'bold' )
    ylabel ('Counts', 'FontSize', 10, 'FontWeight', 'bold' )
    set(gca,'box','off','tickdir','out')
    axis tight
    c_prctile = prctile(c,[0, 95]);
    xlim(c_prctile)
    
    %% Cumulative distributions of choice
    subplot(3,3,8), hold on
    plot(1:length(bhv.ChoiceLeft), cumsum(bhv.ChoiceLeft==1), 'c', 'MarkerSize', 5)
    plot(1:length(bhv.ChoiceLeft), cumsum(bhv.ChoiceLeft==0), 'm', 'MarkerSize', 5)
    xlabel('Trial number', 'FontSize', 10, 'FontWeight', 'bold' )
    ylabel('Choice number', 'FontSize', 10, 'FontWeight', 'bold' )
    hold off
    leg = legend('Left', 'Right', 'Location', 'Best');
    set(leg, 'Box', 'off', 'Fontsize', 8)
    
    if ~isempty(strfind(bhv.ProtocolVersion,'Fix'))
        subplot(3,3,9), hold on
%         edges = linspace(0,max(max(dataParsed.FixTime),max(dataParsed.BrokeFixTime)),20);
        edges = [0:100:max(max(bhv.FixTime),max(bhv.BrokeFixTime)),Inf];
        timeb = histc(bhv.BrokeFixTime, edges);
        timec = histc(bhv.FixTime, edges);
        barb = bar(edges(1:end-1),timeb(1:end-1),'r');
        set(get(barb,'Children'),'facealpha',0.5,'edgealpha',0);
        hold on
        barc = bar(edges(1:end-1),timec(1:end-1),'g');
        set(get(barc,'Children'),'facealpha',0.5,'edgealpha',0);
        xlim([-100 3000])
        xlabel 'Fixation time (ms)'
        ylabel Counts
        legend({'Broken', 'Completed'},'location','northeast','box','off')
        legend boxoff
    end
elseif strcmp(bhv.Protocol,'H2OEXISTS')
    %% Reward and choice computations
    
    valid = find(~isnan(bhv.Rewarded));
    
    rewNdx = find(bhv.Rewarded == 1);
    
    choice_left = find(bhv.ChoiceLeft == 1);
    choice_right = find(bhv.ChoiceLeft == 0);
    
    %% Plots of animal's choice and probability of reward
    
    subplot(3,4,[1, 4]), hold on
    axis([1, length(bhv.ChoiceLeft), 0, 1])
    plot(rewNdx,bhv.ChoiceLeft(rewNdx),'og','markersize',3)
    ax(1) = gca;
    set(ax(1),'box','off','tickdir','out','ytick',[0, 0.5, 1],'yticklabel',{'Right','Miss', 'Left'}); % 'FontSize', 11, 'FontWeight', 'bold' )
    xlabel ('Trial number', 'FontSize', 10, 'FontWeight', 'bold' );
    ylabel ('Choice', 'FontSize', 10, 'FontWeight', 'bold' )
    % str2 = ['Prob:   ', num2str(unique(data.ProbRwdLeft/100)), ',   stim dur:   ', num2str(unique(data.LaserDur)), ' (ms)', ',  stim freq:   ', num2str(unique(data.LaserFreq)), ' (Hz)'];
    % if length(str2) > length(str1)
    %     d = length(str2) - length(str1);
    %     if d > 0
    %         str1 = [str1, zeros(d)];
    %     else
    %         str2 = [str2, zeros(d)];
    %     end
    % end
    annotation('textbox',[.60 .5 .5 .5],'edgecolor','none','String',[bhv.Animal ' ' bhv.Date],'fontsize',20,'fontname','Arial','interpreter','none');
    
    % Running average of choice
    windowSize = 10;
    kernel = ones(1,windowSize)/windowSize;
    avg_left = filter(kernel,1,bhv.ChoiceLeft(valid));
    line(valid, avg_left,'LineWidth',1,'color','k','Parent',ax(1));
    margins;
    
    %% Trial number as a function of time
    subplot(3,3,4), hold on
    trial_init = bhv.TrialInit(1:end)/60000;
    plot(trial_init(rewNdx), rewNdx,'og', 'MarkerSize', 3);
    set(gca,'box','off','tickdir','out')
    ylabel('Trial number', 'FontSize', 10, 'FontWeight', 'bold' )
    xlabel('Time (min)', 'FontSize', 10, 'FontWeight', 'bold' )
    axis tight
    margins;
    
    %% Initiation time distribution
    
    it = ( bhv.TrialInit(1:length(bhv.TrialAvail(1:end-1))) - bhv.TrialAvail(1:end-1) )/1000;
    subplot(3,3,6)
    n_bins = 500;
    hist(it, n_bins)
    xlabel ('Initiation time (s)', 'FontSize', 10, 'FontWeight', 'bold' )
    ylabel ('Counts', 'FontSize', 10, 'FontWeight', 'bold' )
    set(gca,'box','off','tickdir','out')
    axis tight
    it_prctile = prctile(it,[0, 95]);
    xlim(it_prctile)
    
    %% Reaction time distribution
    c = bhv.ReactionTime/1000;
    subplot(3,3,7)
    n_bins = 100;
    hist(c, n_bins)
    xlabel ('Reaction time (s)', 'FontSize', 10, 'FontWeight', 'bold' )
    ylabel ('Counts', 'FontSize', 10, 'FontWeight', 'bold' )
    set(gca,'box','off','tickdir','out')
    axis tight
    c_prctile = prctile(c,[0, 95]);
    xlim(c_prctile)
    
    %% Cumulative distributions of choice
    subplot(3,3,8), hold on
    plot(1:length(bhv.ChoiceLeft), cumsum(bhv.ChoiceLeft==1), 'c', 'MarkerSize', 5)
    plot(1:length(bhv.ChoiceLeft), cumsum(bhv.ChoiceLeft==0), 'm', 'MarkerSize', 5)
    xlabel('Trial number', 'FontSize', 10, 'FontWeight', 'bold' )
    ylabel('Choice number', 'FontSize', 10, 'FontWeight', 'bold' )
    hold off
    leg = legend('Left', 'Right', 'Location', 'Best');
    set(leg, 'Box', 'off', 'Fontsize', 8)
    margins;
    
    if ~isempty(strfind(bhv.ProtocolVersion,'Fix'))
        subplot(3,3,9), hold on
%         edges = linspace(0,max(max(dataParsed.FixTime),max(dataParsed.BrokeFixTime)),20);
        edges = [0:100:max(max(bhv.FixTime),max(bhv.BrokeFixTime)),Inf];
        timeb = histc(bhv.BrokeFixTime, edges);
        timec = histc(bhv.FixTime, edges);
        barb = bar(edges(1:end-1),timeb(1:end-1),'r');
        set(get(barb,'Children'),'facealpha',0.5,'edgealpha',0);
        hold on
        barc = bar(edges(1:end-1),timec(1:end-1),'g');
        set(get(barc,'Children'),'facealpha',0.5,'edgealpha',0);
        xlim([-100 3000])
        xlabel 'Fixation time (ms)'
        ylabel Counts
        legend({'Broken', 'Completed'},'location','northeast','box','off')
        legend boxoff
    end
    
end