function [ dataBhv ] = parseBHV( varargin )
%PARSEBHV   Parses behavioral data contained in a text file following
%PatonLab's standard.

    function [ Interval, IntervalSet, IntervalLong, fixcount ] = roundstim( IntervalPrecise, Scaling )
        %ROUNDSTIM Rounds and normalizes intervals
        %   Takes the n-length vector of intervals in miliseconds IntervalPrecise,
        %   where n is the number of trials in a session, and returns Interval
        %   (n-long vector of Intervals rounded and normalized between 0 and 1),
        %   IntervalSet (unique(Intervals)), and fixcount (how many Intervals had
        %   jitters large enough to get a different rounding, and needed to be
        %   corrected)
        
        Interval = round(IntervalPrecise/(Scaling/100))/100;
        IntervalLong = zeros(size(Interval));
        IntervalLong(IntervalPrecise>(Scaling/2)) = 1; % assumes boundary is half the scaling
        IntervalLong(isnan(Interval)) = nan;
        thresh = 1.5;
        table = tabulate(Interval);
        if ~isempty(table)
            IntervalSet = table(table(:,3)>thresh,1)';
            eliminate = table(table(:,3)<thresh & table(:,3)~=0,1);
            fixcount = 0;
            
            for e = 1:length(eliminate)
                fix_ndx = Interval == eliminate(e);
                difference = abs(IntervalSet - eliminate(e));
                nearest = IntervalSet(difference ==  min(difference));
                if numel(nearest)==1
                    Interval(fix_ndx) = nearest;
                    fixcount = fixcount + sum(fix_ndx);
                else
                    Interval(fix_ndx) = NaN;
                    fixcount = fixcount + sum(fix_ndx);
                    display(['Ambiguous stimulus duration detected at trial ' num2str(find(fix_ndx)) ': ' num2str(IntervalPrecise(fix_ndx)) ' milliseconds.'])
                    display 'Consider replacing Arduino board.'
                end
            end
        else
            fixcount = nan;
            IntervalSet = [];
        end
    end

%% Descriptive fields
if isunix
    slash = '/';
else
    slash = '\';
end

if nargin==1 && isnumeric(varargin{1})
    dataRaw  = varargin{1};
    dataBhv.FileName = 'Unknown';
else
    if nargin == 0
        [dataBhv.FileName,PathName] = uigetfile('*.txt','Select behavioral data file to be read');
        FullPath = [PathName dataBhv.FileName];
    elseif nargin == 1 && ischar(varargin{1})
        FullPath = varargin{1};
        slash_ndx = regexp(FullPath,slash);
        dataBhv.FileName = FullPath(slash_ndx(end)+1:end);
        clear slash_ndx
    end
    try
        dataRaw = dlmread(FullPath);
    catch
        error('PatonLab:bhv','Corrupted text file')
    end
    clear FullPath
end

dataBhv.RawData = dataRaw;
uscore_ndx = regexp(dataBhv.FileName,'_');
if any(dataRaw(:,1)==103)
    dataBhv.Animal = char(dataRaw(dataRaw(:,1)==103,2)');
else
    try
        dataBhv.Animal = dataBhv.FileName(1:uscore_ndx(1)-1);
    catch
        warning('PatonLab:bhv', 'Unknown animal name')
        dataBhv.Animal = 'Unknown';
    end
end

if any(dataRaw(:,1)==125)
    dataBhv.Experimenter = char(dataRaw(dataRaw(:,1)==125,2)');
else
    try
        dataBhv.Experimenter = dataBhv.FileName(uscore_ndx(3)+1:uscore_ndx(3)+2);
    catch
        warning('PatonLab:bhv', 'Unknown experimenter name')
        dataBhv.Experimenter = 'Unknown';
    end
end

if any(dataRaw(:,1)==126)
    Protocol = char(dataRaw(dataRaw(:,1)==126,2)');
else
    try
        Protocol = dataBhv.FileName(uscore_ndx(1)+1:uscore_ndx(2)-1);
    catch
        warning('PatonLab:bhv', 'Unknown task name')
        Protocol = 'Unknown';
    end
end
if ismember('v',Protocol)
    dataBhv.Protocol = Protocol(1:find(Protocol=='v')-1);
    dataBhv.ProtocolVersion = Protocol(find(Protocol=='v')+1:end);
else
    dataBhv.Protocol = Protocol;
    dataBhv.ProtocolVersion = 'Unknown';
end
if strcmp('2AFC',dataBhv.Protocol)
    dataBhv.Protocol = 'TAFC';
end

if any(dataRaw(:,1)==127)
    dataBhv.Date = char(dataRaw(dataRaw(:,1)==127,2)');
else
    try
        dataBhv.Date = dataBhv.FileName(uscore_ndx(2)+1:uscore_ndx(2)+6); % YYMMDD;
    catch
        warning('PatonLab:bhv', 'Unknown date')
        dataBhv.Date = 'Unknown';
    end
end

if regexp(dataBhv.FileName,'YFP')
    dataBhv.Transgene = 'YFP';
elseif regexp(dataBhv.FileName,'ChR2')
    dataBhv.Transgene = 'ChR2';
elseif regexp(dataBhv.FileName,'Arch')
    dataBhv.Transgene = 'Arch';
else
    dataBhv.Transgene = 'none';
end

if any(dataRaw(:,1)==123)
    dataBhv.Treatment = char(dataRaw(dataRaw(:,1)==123,2)');
end
if any(dataRaw(:,1)==124)
    dataBhv.TreatmentDose = eval(char(dataRaw(dataRaw(:,1)==124,2)'));
end


%% Session-based variables
if strcmp('TAFC',dataBhv.Protocol)
    if any(dataRaw(:,1)==106)
        dataBhv.Scaling = dataRaw(find(dataRaw(:,1)==106,1),2);
    else
        dataBhv.Scaling = 3000;
    end
    if sum(dataRaw(:,1)==133) == 1
        dataBhv.LeftLong = dataRaw(dataRaw(:,1)==133,2);
    end
end

%% Trial-based variables
timeStamps_ndx = find(dataRaw(:,1)<=100 | dataRaw(:,1)>200);
dataRaw(timeStamps_ndx,2) = dataRaw(timeStamps_ndx,2)-dataRaw(timeStamps_ndx(1),2);

state0_ndx = find(dataRaw(:,1)==22);
dataBhv.TrialNumber = 1:length(state0_ndx);
blockCount = 0;

for t = 1:length(state0_ndx)
    if t < length(state0_ndx)
        t_data = dataRaw(state0_ndx(t):state0_ndx(t+1),:);
    else
        t_data = dataRaw(state0_ndx(t):end,:);
    end
    
    try
        dataBhv.TrialAvail(t) = t_data(t_data(:,1)==23,2);
    catch
        %         error('PatonLab:bhv','Error at detecting trial availability times: can''t find wait_Cin state 23.')
        dataBhv.TrialAvail(t) = nan;
    end
    
    dataBhv.PokeTimes.LeftIn{t} = t_data(t_data(:,1)==1,2);
    dataBhv.PokeTimes.LeftOut{t} = t_data(t_data(:,1)==9,2);
    dataBhv.PokeTimes.RightIn{t} = t_data(t_data(:,1)==2,2);
    dataBhv.PokeTimes.RightOut{t} = t_data(t_data(:,1)==10,2);
    dataBhv.PokeTimes.CenterIn{t} = t_data(t_data(:,1)==0,2);
    dataBhv.PokeTimes.CenterOut{t} = t_data(t_data(:,1)==8,2);
    
    if ismember(40,t_data(:,1))
        dataBhv.ChoiceMiss(t) = true;
    else
        dataBhv.ChoiceMiss(t) = false;
    end
    
    if ismember(54,t_data(:,1))
        dataBhv.RwdMiss(t) = true;
    else
        dataBhv.RwdMiss(t) = false;
    end
    
    if any(ismember(t_data(:,1),[26:29]))
        try
            dataBhv.ReactionTime(t) = t_data(ismember(t_data(:,1),[26:29]),2)-t_data(t_data(:,1)==25,2); % duration of wait_Sin state
            %             dataParsed.AlignOn.Choice(t) = t_data(ismember(t_data(:,1),[26:29]),2);
        catch
            %             error('PatonLab:bhv','Error calculating choice time: more than one choice state detected in a single trial.')
            dataBhv.ReactionTime(t) = nan;
        end
        try
            dataBhv.AlignOn.Choice(t) = t_data(find((t_data(:,1)==1|t_data(:,1)==2)&(t_data(:,2)>t_data((t_data(:,1)==16|t_data(:,1)==73),2)),1),2); % first side poke after stimulus onset
        catch
            dataBhv.AlignOn.Choice(t) = nan;
        end
        
        %dataParsed.ReactionTime = t_data( find(and(ismember(t_data(:,1),[1 2]),t_data(:,2)>t_data(t_data(:,1)==17,2)),1) ,2) - t_data(t_data(:,1)==17,2); % find first side poke after 2nd beep
    else
        dataBhv.ReactionTime(t) = nan;
        dataBhv.AlignOn.Choice(t) = nan;
    end
    
    %     if any(ismember(t_data(:,1),23))
    
    % end
    dataBhv.AlignOn.Random(t) = randi([min(t_data(:,2)), max(t_data(:,2))],1);
    
    if any(ismember(t_data(:,1),61))
        dataBhv.SyncPulse(t) = t_data(t_data==61,2);
    else
        dataBhv.SyncPulse(t) = nan;
    end
    
    if any(ismember(t_data(:,1),[6, 7]))
        dataBhv.AlignOn.Reward(t) = t_data(t_data(:,1)==6|t_data(:,1)==7,2);
    else
        dataBhv.AlignOn.Reward(t) = nan;
    end
    
    if any(ismember(t_data(:,1),[83, 205, 207]))
        ndx = find(t_data(:,1)==83|t_data(:,1)==205|t_data(:,1)==207,1);
        dataBhv.AlignOn.Laser(t) = t_data(ndx,2);
    elseif isfield(dataBhv.AlignOn,'Laser')
        dataBhv.AlignOn.Laser(t) = nan;
    end
    
    %% Task specific fields - MATCHING
    if strcmp('MATCHING',dataBhv.Protocol)
        
        try
            dataBhv.TrialInit(t) = t_data(t_data(:,1)==11,2);
        catch
            dataBhv.TrialInit(t) = NaN;
        end
        
        if ismember(26,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 1;
            dataBhv.Rewarded(t) = 1;
        elseif ismember(27,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 0;
            dataBhv.Rewarded(t) = 0;
        elseif ismember(28,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 0;
            dataBhv.Rewarded(t) = 1;
        elseif ismember(29,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 1;
            dataBhv.Rewarded(t) = 0;
        else
            dataBhv.ChoiceLeft(t) = NaN;
            dataBhv.Rewarded(t) = NaN;
        end
        
        if ismember(101,t_data)
            blockCount = blockCount + 1;
            dataBhv.ProbRwdLeft(t) = t_data(t_data(:,1)==101,2);
            dataBhv.ProbRwdRight(t) = t_data(t_data(:,1)==102,2);
        else
            try
                dataBhv.ProbRwdLeft(t) = dataBhv.ProbRwdLeft(t-1);
                dataBhv.ProbRwdRight(t) = dataBhv.ProbRwdRight(t-1);
            catch
                dataBhv.ProbRwdLeft(t) = NaN;
                dataBhv.ProbRwdRight(t) = NaN;
            end
        end
        
        if dataBhv.ProbRwdLeft(t) > dataBhv.ProbRwdRight(t)
            dataBhv.LeftHigh(t) = 1;
        elseif dataBhv.ProbRwdLeft(t) < dataBhv.ProbRwdRight(t)
            dataBhv.LeftHigh(t) = 0;
        else
            dataBhv.LeftHigh(t) = NaN;
        end
        
        dataBhv.BlockNumber(t) = blockCount;
        
        if ismember(83,t_data(:,1)) % 83 = laser_onset
            dataBhv.IsLaser(t) = 1;
            dataBhv.LaserDur(t) = t_data(t_data(:,1)==105,2);
            if ismember(113,t_data(:,1))
                dataBhv.LaserFreq(t) = t_data(t_data(:,1)==113,2); %added by Marina, 30/07/2013
            else
                dataBhv.LaserFreq(t) = NaN;
            end
        else
            dataBhv.IsLaser(t) = 0;
            dataBhv.LaserDur(t) = NaN;
            dataBhv.LaserFreq(t) = NaN; %added by Marina, 30/07/2013 % 0s to NANs by Thiago, 14/12/2013
        end
        
        if ~isempty(strfind(dataBhv.ProtocolVersion,'Fix'))
            try
                dataBhv.TrialInit(t) = t_data(t_data(:,1)==73,2);
            catch
                dataBhv.TrialInit(t) = NaN;
            end
            if ismember(75,t_data(:,1))
                dataBhv.BrokeFix(t) = 1;
                dataBhv.BrokeFixTime(t) = t_data(t_data(:,1)==75,2) - dataBhv.TrialInit(t);
                dataBhv.FixTime(t) = nan;
            else
                dataBhv.BrokeFix(t) = 0;
                dataBhv.BrokeFixTime(t) = nan;
                try
                    dataBhv.FixTime(t) = t_data(t_data(:,1)==25,2) - dataBhv.TrialInit(t);
                catch
                    dataBhv.FixTime(t) = nan;
                end
            end
        end
        
        if any(t_data(:,1)==6)%Rui changed 66 to 6
            dataBhv.RewardDuration(t) = t_data(t_data(:,1)==14,2) - t_data(t_data(:,1)==6,2);
        elseif any(t_data(:,1)==7)
            dataBhv.RewardDuration(t) = t_data(t_data(:,1)==15,2) - t_data(t_data(:,1)==7,2);
        else
            dataBhv.RewardDuration(t) = nan;
        end
        % if ismember(6,t_data(:,1))
        
        
        %% Task specific fields - TAFC
    elseif strcmp('TAFC',dataBhv.Protocol)
        try
            dataBhv.TrialInit(t) = t_data(t_data(:,1)==11,2);
        catch
            dataBhv.TrialInit(t) = NaN;
        end
        
        %         if strcmp(dataParsed.ProtocolVersion(1:3),'Fix')
        if isfield(dataBhv,'ProtocolVersion') && ~isempty(strfind(dataBhv.ProtocolVersion,'Fix'))
            if ismember(75,t_data(:,1))
                dataBhv.BrokeFix(t) = 1;
                dataBhv.BrokeFixTime(t) = t_data(t_data(:,1)==75,2) - dataBhv.TrialInit(t);
            else
                dataBhv.BrokeFix(t) = 0;
                dataBhv.BrokeFixTime(t) = nan;
            end
        else
            if ismember(37,t_data(:,1))
                dataBhv.Premature(t) = 1;
                dataBhv.PrematureLong(t) = NaN;
            elseif ismember(38,t_data(:,1))
                dataBhv.Premature(t) = 1;
                dataBhv.PrematureLong(t) = 1;
            elseif ismember(39,t_data(:,1))
                dataBhv.Premature(t) = 1;
                dataBhv.PrematureLong(t) = 0;
            else
                dataBhv.Premature(t) = 0;
                dataBhv.PrematureLong(t) = NaN;
            end
            
            if dataBhv.Premature(t)==1
                try
                    dataBhv.PremTime(t) = t_data(ismember(t_data(:,1),[37:39 48:51]),2)-t_data(t_data(:,1)==16,2);
                catch
                    dataBhv.PremTime(t) = NaN;
                    display 'Error: Two premature responses detected in a single trial'
                end
            else
                dataBhv.PremTime(t) = NaN;
            end
        end
        
        if ismember(26,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 1;
            dataBhv.ChoiceCorrect(t) = 1;
        elseif ismember(27,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 0;
            dataBhv.ChoiceCorrect(t) = 0;
        elseif ismember(28,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 0;
            dataBhv.ChoiceCorrect(t) = 1;
        elseif ismember(29,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 1;
            dataBhv.ChoiceCorrect(t) = 0;
        else
            dataBhv.ChoiceLeft(t) = NaN;
            dataBhv.ChoiceCorrect(t) = NaN;
        end
        
        if ismember(17,t_data(:,1)) && ismember(16,t_data(:,1))
            dataBhv.IntervalPrecise(t) = t_data(find(t_data(:,1)==17,1),2)-t_data(find(t_data(:,1)==16,1),2);
        elseif ismember(17,t_data(:,1)) && ismember(56,t_data(:,1))
            dataBhv.IntervalPrecise(t) = t_data(find(t_data(:,1)==17,1),2)-t_data(find(t_data(:,1)==56,1),2);
        else
            dataBhv.IntervalPrecise(t) = NaN;
        end
        
        if ismember(20,t_data(:,1)) && (ismember(30,t_data(:,1)) || ismember(31,t_data(:,1)))
            dataBhv.StimRwdDelay(t) = t_data(find(t_data(:,1)==30|t_data(:,1)==31,1),2) - t_data(find(t_data(:,1)==20|t_data(:,1)==4|t_data(:,1)==5|t_data(:,1)==25,1),2);
        elseif ismember(25,t_data(:,1)) && (ismember(30,t_data(:,1)) || ismember(31,t_data(:,1)))
            dataBhv.StimRwdDelay(t) = t_data(find(t_data(:,1)==30|t_data(:,1)==31,1),2) - t_data(find(t_data(:,1)==25,1),2);
        else
            dataBhv.StimRwdDelay(t) = NaN;
        end
        
        if any(ismember(t_data(:,1),56))
            try
                dataBhv.Latency(t) = t_data(find(t_data(:,1)==56,1),2) - t_data(find(t_data(:,1)==61,1),2);
            catch
                dataBhv.Latency(t) = nan;
            end
        else
            dataBhv.Latency(t) = nan;
        end
        % Align on stimulus
        try
            dataBhv.AlignOn.StimOn(t) = t_data(t_data(:,1)==16,2);
        catch
            dataBhv.AlignOn.StimOn(t) = nan;
        end
        try
            dataBhv.AlignOn.StimOff(t) = t_data(t_data(:,1)==20,2);
        catch
            try
                dataBhv.AlignOn.StimOff(t) = t_data(t_data(:,1)==17,2);
            catch
                dataBhv.AlignOn.StimOff(t) = nan;
            end
        end
        
        %%----MEN--AT--WORK----
        if ismember(133,t_data)
            blockCount = blockCount + 1;
            dataBhv.LeftLong(t) = t_data(t_data(:,1)==133,2);
        else
            try
                dataBhv.LeftLong(t) = dataBhv.LeftLong(t-1);
            end
        end
        
        if isfield(dataBhv,'LeftLong')
            if isnan(dataBhv.ChoiceLeft(t))
                dataBhv.ChoiceLong(t) = nan;
            else
                if logical(dataBhv.LeftLong(t))
                    dataBhv.ChoiceLong(t) = dataBhv.ChoiceLeft(t);
                else
                    dataBhv.ChoiceLong(t) = double(~(dataBhv.ChoiceLeft(t)));
                end
            end            
        end
        if blockCount > 0
            dataBhv.BlockNumber(t) = blockCount;
        end
        %%----MEN--AT--WORK----
        
        %% Task specific fields - H2OEXISTS
    elseif strcmp('H2OEXISTS',dataBhv.Protocol)
        
        if ismember(26,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 1;
            dataBhv.Rewarded(t) = 1;
        elseif ismember(27,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 0;
            dataBhv.Rewarded(t) = 0;
        elseif ismember(28,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 0;
            dataBhv.Rewarded(t) = 1;
        elseif ismember(29,t_data(:,1))
            dataBhv.ChoiceLeft(t) = 1;
            dataBhv.Rewarded(t) = 0;
        else
            dataBhv.ChoiceLeft(t) = NaN;
            dataBhv.Rewarded(t) = NaN;
        end
        
        if ismember(101,t_data)
            blockCount = blockCount + 1;
            dataBhv.ProbRwdLeft(t) = t_data(t_data(:,1)==101,2);
            dataBhv.ProbRwdRight(t) = t_data(t_data(:,1)==102,2);
        else
            try
                dataBhv.ProbRwdLeft(t) = dataBhv.ProbRwdLeft(t-1);
                dataBhv.ProbRwdRight(t) = dataBhv.ProbRwdRight(t-1);
            catch
                dataBhv.ProbRwdLeft(t) = NaN;
                dataBhv.ProbRwdRight(t) = NaN;
            end
        end
        
        if dataBhv.ProbRwdLeft(t) > dataBhv.ProbRwdRight(t)
            dataBhv.LeftHigh(t) = 1;
        elseif dataBhv.ProbRwdLeft(t) < dataBhv.ProbRwdRight(t)
            dataBhv.LeftHigh(t) = 0;
        else
            dataBhv.LeftHigh(t) = NaN;
        end
        
        dataBhv.BlockNumber(t) = blockCount;
                       
        try
            dataBhv.TrialInit(t) = t_data(t_data(:,1)==26|t_data(:,1)==28,2);
        catch
            dataBhv.TrialInit(t) = NaN;
        end
        
        try
            dataBhv.TrialAvail(t) = t_data(t_data(:,1)==25,2);
        catch
            dataBhv.TrialAvail(t) = NaN;
        end
                
        if any(t_data(:,1)==6)%Rui changed 66 to 6
            dataBhv.RewardDuration(t) = t_data(t_data(:,1)==14,2) - t_data(t_data(:,1)==6,2);
        elseif any(t_data(:,1)==7)
            dataBhv.RewardDuration(t) = t_data(t_data(:,1)==15,2) - t_data(t_data(:,1)==7,2);
        else
            dataBhv.RewardDuration(t) = nan;
        end
    end
    
    %% Session-based variables
    
    if strcmp('TAFC',dataBhv.Protocol)
        [dataBhv.Interval, dataBhv.IntervalSet, dataBhv.IntervalLong, fixcount] = roundstim(dataBhv.IntervalPrecise,dataBhv.Scaling);
        if fixcount > 0
            display(['Forced rounding needed in ' num2str(fixcount) ' trials.'])
        end
        A = fieldnames(dataBhv.AlignOn);
        for a = 1:length(A)
            dataBhv.AlignOn.(A{a}) = dataBhv.AlignOn.(A{a}) - dataBhv.SyncPulse;
        end
    elseif strcmp('MATCHING',dataBhv.Protocol) || strcmp('H2OEXISTS',dataBhv.Protocol)
        timeValvL = mean(dataRaw(dataRaw(:,1)==14,2)-dataRaw(dataRaw(:,1)==6,2));
        timeValvR = mean(dataRaw(dataRaw(:,1)==15,2)-dataRaw(dataRaw(:,1)==7,2));
        %     display(['Average valve opening time per reward: ' num2str(timeValvL) 'ms (left), ' num2str(timeValvR) 'ms (right).' ])
    end
    %%
    dataBhv = orderfields(dataBhv);
end
end