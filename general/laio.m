function [ rho, delta ] = laio( data, thdist )
%LAIO Clustering by density peaks
%
%   After:
%   A. Rodriguez, A. Laio (2014) Clustering by fast search and find of
%   density peaks. Science 344 (6191), 1492-1496.
%   DOI: 10.1126/science.1242072

narginchk(1,2)
if size(data,1)>1000
    data = data(randperm(size(data,1),1000),:);
end

D = squareform(pdist(data));

if nargin < 2
    thdist = prctile(D(:),2);
end

%%
rho = sum((D-thdist)<0)'-1; % equation (1), minus self
delta = nan(size(rho));
delta(rho==max(rho)) = max(D(:));
for i = find(rho~=max(rho))'
    d = D(i,:);
    delta(i) = min(d(rho>rho(i)));
end

%% Plotting
% figure,
% plot(rho, delta, '.')
% xlabel(texlabel('rho'))
% ylabel(texlabel('delta'))

end

