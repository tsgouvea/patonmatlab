function [ RF, ST, FR ] = movinggauss_offset( nNeurons, tOffset, dFR, sigmaT, tOnset)%, bFR, gain )
%LUDVIG Simulates neural data that looks like Ludvig et al., 2008
%
%   INPUTS
%   nNeurons = number of neurons
%   tMax = number of timesteps
%   (optional) dFR = desired average firing rate; standard = 20;
%   (optional) sigmaT = decay rate calculated from tMax + gaussian noise
%   with std sigmaT; standard = zero noise.
%   (optional) bFR = baseline firing rate; standard = 0;
%
%   OUTPUTS
%   RF = rate functions
%   ST = spike trains derived from RF scaled to match dFR
%   FR = running average of ST, i.e., ST convolved with a gaussian kernel
%   of std = tMax/100

narginchk(2,5)
if nargin < 5
    tOnset = 0;
end
if nargin <= 2
    dFR = 20;
end
T = -tOnset:tOffset;
sigma = .1;
if nargin < 4
    RF = normpdf(ones(nNeurons,1)*T,tOnset+linspace(0,tOffset,nNeurons)'*ones(1,length(T)),tOffset*sigma);
else
    RF = normpdf(ones(nNeurons,1)*linspace(0,tOffset,length(T))+randn*sigmaT,tOnset+linspace(0,tOffset,nNeurons)'*ones(1,length(T)),tOffset*sigma);
end

RF = RF/mean(RF(:))/1000;
if isscalar(dFR)
    RF = RF*dFR;
    bRF = (ones(size(RF))/1000).*(dFR*ones(nNeurons,length(T)));
else
    dFR = dFR(:);
    RF = RF.*(dFR*ones(1,length(T)));
    bRF = (ones(size(RF))/1000).*(dFR*ones(1,length(T)));
end

RF = (RF+bRF)/2;

% if nargin >= 6
%     gain = gain(:);
%     RF = RF/mean(RF(:)) .* (gain*ones(1,length(T))*dFR/1000);
% else
%     RF = RF/mean(RF(:)) * dFR/1000;
% end
% if nargin >= 5
%     RF = RF + ones(size(RF))/1000*bFR;
% end
if nargout >=2
    ST = double(rand(size(RF)) < RF );
%     imagesc(1-ST), colormap gray
end
if nargout >=3
    ksigma = tOffset/100;
    kernel = normpdf(-3*ksigma:3*ksigma,0,ksigma); kernel = kernel/sum(kernel(:));
    FR = conv2(ST,kernel,'same');
%     imagesc(FR), colormap jet
end
end

