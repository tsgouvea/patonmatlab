function [ sorted_matrix, sorted_indices ] = sortbypc( varargin )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

matrix = varargin{1};
if nargin==1
    plo = false;
    fract = 1;
elseif nargin>=2
    fract = varargin{2};
    plo = false;
    if fract>1 || fract<0
        display('Second argument must be a value between 0 and 1');
        return
        fract = 1;
    end
    if nargin>=3
        plo = varargin{3};
        if ~islogical(plo)
            display('Third argument must be a boolean. Type ''true'' if you, like Joe, want to see plots.')
            plo = false;
        end
    end
end

[coeff,score,~,~,explained] = pca(matrix);
[angpos, ecce] = cart2pol(score(:,1),score(:,2));
% figure, polar(angpos, dist,'.')

[~, sorted_indices] = sortrows(angpos);

ecce_ndx = ecce(sorted_indices) >= prctile(ecce,(1-fract)*100);

sorted_indices(~ecce_ndx) = [];

sorted_matrix = matrix(sorted_indices,:);

if plo
    figure, plot(coeff(:,1:3))
    legend({'1st PC','2nd PC', '3rd PC'},'location','best')
    legend boxoff
    figure, plot(score(:,1),score(:,2),'.')
    xlabel '1st PC'
    ylabel '2nd PC'
    figure, plot(cumsum(explained),'k*','markersize',3)
    xlabel('Principal components')
    ylabel('Variance explained (cumulative)')
end

end