function [ RF, ST, FR ] = movinggauss( nNeurons, tOffset, varargin)%, bFR, gain )
%LUDVIG Simulates neural data that looks like Ludvig et al., 2008
%
%   INPUTS
%   nNeurons = number of neurons
%   tMax = number of timesteps
%   (optional) dFR = desired average firing rate; standard = 20;
%   (optional) sigmaT = decay rate calculated from tMax + gaussian noise
%   with std sigmaT; standard = zero noise.
%   (optional) bFR = baseline firing rate; standard = 0;
%
%   OUTPUTS
%   RF = rate functions
%   ST = spike trains derived from RF scaled to match dFR
%   FR = running average of ST, i.e., ST convolved with a gaussian kernel
%   of std = tMax/100

%% Parsing inputs
p = inputParser;

% nNeurons, tOffset, tOnset, dFR, SNR, sigma_slope, sigma_offset
addRequired(p,'nNeurons',@(x) isnumeric(x) && isscalar(x) && (x > 0));
addRequired(p,'tOffset',@(x) isnumeric(x) && isscalar(x) && (x > 0));
addOptional(p,'tOnset',0,@(x) isnumeric(x) && isscalar(x) && (x >= 0));
addOptional(p,'dFR',100,@isnumeric);
addOptional(p,'SNR',1,@(x) isnumeric(x) && isscalar(x) && (x > 0));
addOptional(p,'sigma_slope',0,@(x) isnumeric(x) && isscalar(x) && (x >= 0));
addOptional(p,'sigma_offset',0,@(x) isnumeric(x) && isscalar(x) && (x >= 0));
% addOptional(p,'sigma_RF',tOffset*.1,@(x) isnumeric(x) && isscalar(x) && (x > 0));

parse(p,nNeurons,tOffset,varargin{:})
for iVar = fieldnames(p.Results)'
    eval([iVar{1} '=p.Results.' iVar{1} ';'])
end

if isscalar(dFR)
    dFR = ones(nNeurons,1)*dFR;
else
    dFR = dFR(:);
end

%%
slope = 1 + randn*sigma_slope/1000;
offset = randn*sigma_offset;
sigma_RF = 0.1;
T = -tOnset:tOffset;
RF = normpdf(ones(nNeurons,1)*T,offset + linspace(-tOnset*slope,tOffset*slope,nNeurons)'*ones(1,length(T)),(tOffset+tOnset)*sigma_RF);
RF = RF*(tOnset+tOffset)/1000; % Normalized to 1 Hz
RF = (RF+ones(size(RF))/1000)/2; % FIX IT Assumes what I meant by SNR = 1
RF = RF.*(dFR*ones(1,length(T))); % Applies desired firing rates

if nargout >=2
    ST = double(rand(size(RF)) < RF );
%     imagesc(1-ST), colormap gray, axis square
end
if nargout >=3
    ksigma = (tOffset+tOnset)/100;
    kernel = normpdf(-3*ksigma:3*ksigma,0,ksigma); kernel = kernel/sum(kernel(:));
    FR = conv2(ST,kernel,'same');
%     imagesc(FR), colormap jet
end
end

