function [ RF, ST, FR ] = ludvig( nNeurons, tMax, dFR, sigmaT, bFR )
%LUDVIG Simulates neural data that looks like Ludvig et al., 2008
%
%   INPUTS
%   nNeurons = number of neurons
%   tMax = number of timesteps
%   (optional) dFR = desired average firing rate; standard = 20;
%   (optional) sigmaT = decay rate calculated from tMax + gaussian noise
%   with std sigmaT; standard = zero noise.
%   (optional) bFR = baseline firing rate; standard = 0;
%
%   OUTPUTS
%   RF = rate functions
%   ST = spike trains derived from RF scaled to match dFR
%   FR = running average of ST, i.e., ST convolved with a gaussian kernel
%   of std = tMax/100

narginchk(2,5)
if nargin <= 2
    dFR = 20;
end
T = 0:tMax;
sigma = .2;
if nargin < 4
    gamma = exp(log(.4)/tMax); % .2 is a lower asymptote for trace;  .999;
else
    gamma = exp(log(.4)/(tMax+randn*sigmaT)); % .2 is a lower asymptote for trace;  .999;
end
n = linspace(1,0,nNeurons)';
trace = gamma.^T;
% trace = linspace(1,0,length(T));
Y = ones(size(n))*trace;
Mu = n*ones(size(trace));
RF = exp(((Y-Mu).^2)/(-2*sigma^2))/sqrt(2*pi);%;.*Y;
RF = RF/mean(RF(:))*dFR/1000;%/1000;
if nargin >= 5
    RF = RF + ones(size(RF))/1000*bFR;
end
if nargin >=2
    ST = double(rand(size(RF)) < RF );
%     imagesc(1-ST), colormap gray
end
if nargin >=3
    ksigma = tMax/100;
    kernel = normpdf(-3*ksigma:3*ksigma,0,ksigma); kernel = kernel/sum(kernel(:));
    FR = conv2(ST,kernel,'same');
%     imagesc(FR), colormap jet
end
end

