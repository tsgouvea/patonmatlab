function [ output_args ] = shaded(xvalues, ysignal, ynoise, lcolor, shalpha, lwid )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

narginchk(2,6)

if nargin < 6
    lwid = 2;
    if nargin < 5
        shalpha = .5;
        if nargin < 4
            lcolor = [0.0431    0.3725    0.6471];
            if nargin < 3
                xvalues = 1:length(ysignal);
            end
        end
    end
end
shcolor = tanh(lcolor+1);

patch([xvalues fliplr(xvalues)],[ysignal+ynoise fliplr(ysignal-ynoise)],shcolor,'edgecolor','none','facealpha',shalpha)
hold on
plot(xvalues,ysignal,'color',lcolor,'linewidth',lwid)

end

