function [ D_kl ] = kldiv( P, Q )
%KLDIV Kullback-Leibler Divergence of Q from P
%   Detailed explanation goes here
P = P(:);
Q = Q(:);

D_kl = sum(P.*(log(P./Q)));

end

