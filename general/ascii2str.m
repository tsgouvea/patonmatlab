function [ str ] = ascii2str( ascii )
%ASCII2STR Converts ascii into string
%   ascii is a N-by-1 vector in which every 3 digits form one ASCII character,
%   represented in decimal numbers

ascii = ascii(:);
str = char(ascii');

end