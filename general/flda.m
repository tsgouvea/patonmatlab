% rFLDA - MATLAB subroutine to perform regularized Fisher's linear discriminant analysis
%
% Use:
% W = rFLDA2(Input,Target,Lam,Priors)
%
% W       = discovered linear coefficients 
% Input   = predictor data (size(Input) = <NxD>) 
% Target  = target variable (class labels)
% Lam     = weighting factor for regularization term
% Priors  = vector of prior probabilities (optional)
%
% Note: discriminant coefficients are stored in W in the order of unique(Target)
%

function W = rFLDA(Input,Target,Lam,Priors)

% Determine size of input data
[n, m] = size(Input);

% Discover and count unique class labels
ClassLabel = unique(Target);
k = length(ClassLabel);

% Initialize
nGroup     = NaN(k,1);     % Group counts
GroupMean  = NaN(k,m);     % Group sample means
Sw         = zeros(m,m);   % Pooled covariance
W          = NaN(k,m+1);   % model coefficients

%% Assign prior probabilities
if  (nargin >= 4)
    % Use the user-supplied priors
    PriorProb = Priors;
else
    % Use the sample probabilities
    PriorProb = [sum(ClassLabel(1) == Target) sum(ClassLabel(2) == Target)] / n;
end

%% Loop over classes to perform intermediate calculations
for i = 1:k,
    % Establish location and size of each class
    Group      = (Target == ClassLabel(i));
    nGroup(i)  = sum(double(Group));
    
    % Calculate group mean vectors
    GroupMean(i,:) = mean(Input(Group,:));
    
    % Accumulate pooled covariance information
    Sw = Sw + PriorProb(i) .* cov(Input(Group,:));
end

%% Compute linear discriminant coefficients
Temp = eye(size(Sw)) / (Sw + Lam * eye(size(Sw)));
W = Temp * (GroupMean(1,:)' - GroupMean(2,:)');

%% Housekeeping
clear Temp

end





