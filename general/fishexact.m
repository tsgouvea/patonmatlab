function [ pval, chi2pval ] = fishexact( varargin )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% if numel(varargin) == 1
%     M = varargin{1};    
% else
    [M,~,chi2pval] = crosstab(varargin{1}(:),varargin{2}(:));
% end
a = M(1,1);
b = M(1,2);
c = M(2,1);
d = M(2,2);
pval = (factorial(a+b) * factorial(c+d) * factorial(a+c) * factorial(b+d))/...
    (factorial(a) * factorial(b) * factorial(c) * factorial(d) * factorial(a+b+c+d));
end

