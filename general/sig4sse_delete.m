function [ error ] = sig4sse_delete(param, X, Y )
%SIG4SSE Sum of squared errors between f(X,param) and (X,Y)
%   f(X,param) = param(3) + param(4)./(1+exp(-1*((X-param(2))*param(1)));

Yhat = param(1) + param(2)./(1+exp(-1*(X-param(3))*param(4)));
error = nansum((Y-Yhat).^2);

end

