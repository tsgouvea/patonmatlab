function [myxlim, myylim, myzlim ] = margins( ha, mx, my, mz, gap )
%MARGINS Adjusts limits of axis to look good
%   Detailed explanation goes here
narginchk(0,5)

if nargin < 5
    gap = 0.1;
    if nargin < 4
        mz = false;
        if nargin < 3
            my = true;
            if nargin < 2
                mx = true;
                if nargin == 0
                    ha = gca;
                end
            end
        end
    end
end

old_xlim = get(ha,'xlim');
old_ylim = get(ha,'ylim');
old_zlim = get(ha,'zlim');
axes(ha)
axis tight
myxlim = get(ha,'xlim');
myylim = get(ha,'ylim');
myzlim = get(ha,'zlim');

if mx
    myxamp = diff(myxlim);
    if strcmp(get(ha,'xscale'),'linear')
        myxlim(1) = myxlim(1) - gap*myxamp;
        myxlim(2) = myxlim(2) + gap*myxamp;
        set(ha,'xlim',myxlim)
    elseif strcmp(get(ha,'xscale'),'log')
        myxlim(1) = old_xlim(2) - myxamp*exp(gap)^log(myxamp);
        myxlim(2) = old_xlim(1) + myxamp*exp(gap)^log(myxamp);
        myxlim(myxlim<0) = 0;
        set(ha,'xlim',myxlim)
    end
end
if my
    myyamp = diff(myylim);
    if strcmp(get(ha,'yscale'),'linear')
        myylim(1) = myylim(1) - gap*myyamp;
        myylim(2) = myylim(2) + gap*myyamp;
        set(ha,'ylim',myylim)
    elseif strcmp(get(ha,'yscale'),'log')
        myylim(1) = old_ylim(2) - myyamp*exp(gap)^log(myyamp);
        myylim(2) = old_ylim(1) + myyamp*exp(gap)^log(myyamp);
        myylim(myylim<0) = 0;
        set(ha,'ylim',myylim)
    end
end
if mz
    myzamp = diff(myzlim);
    if strcmp(get(ha,'zscale'),'linear')
        myzlim(1) = myzlim(1) - gap*myzamp;
        myzlim(2) = myzlim(2) + gap*myzamp;
        set(ha,'zlim',myzlim)
    elseif strcmp(get(ha,'zscale'),'log')
        myzlim(1) = old_zlim(2) - myzamp*ezp(gap)^log(myzamp);
        myzlim(2) = old_zlim(1) + myzamp*ezp(gap)^log(myzamp);
        myzlim(myzlim<0) = 0;
        set(ha,'zlim',myzlim)
    end
end
if ~mx
    set(ha,'xlim',old_xlim)
end
if ~my
    set(ha,'ylim',old_ylim)
end
if ~mz
    set(ha,'zlim',old_zlim)
end
end

