function [ u ] = minmax( v )
%MINMAX Min-max normalization
%   Detailed explanation goes here
u = (v-min(v(:))) / (max(v(:))-min(v(:)));

end

