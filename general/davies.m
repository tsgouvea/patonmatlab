function [ R, Rmin ] = davies(wf, ulabel, toplot)
%DAVIES Davies's cluster separation measure
%   Davies and Bouldin, 1979, IEEE
%   Detailed explanation goes here

narginchk(2,3)

if nargin < 3
    toplot = false;
end

%% Synth
% wf = randn(1000,48)*.05;
% ulabel = randi(3,1000,1)-1;
% wf(ulabel==0,:) = wf(ulabel==0,:) + repmat(-normpdf([1:48],12,3)*.5,sum(ulabel==0),1);
% wf(ulabel==1,:) = wf(ulabel==1,:) + repmat(-normpdf([1:48],12,3)+normpdf([1:48],8,2)*.7+normpdf([1:48],15,2)*.7,sum(ulabel==1),1);
% wf(ulabel==2,:) = wf(ulabel==2,:) + repmat(-normpdf([1:48],12,2)*.8+normpdf([1:48],18,6),sum(ulabel==2),1);
% ulabel(ulabel==2&rand(size(ulabel))<.5) = 3;
% wf(ulabel==3,:) = circshift(wf(ulabel==3,:)',0)'*.7;

%% Action!
U = double(unique(ulabel));
U = U(U<255);

for u = U
    uNdx = find(ulabel==u);
    numUnits = length(uNdx);
    if numUnits > 10000
        uNdx = randsample(uNdx,10000);
        numUnits = 10000;
    end
    A(:,u==U) = mean(wf(uNdx,:));    
    tocentroid = pdist([A(:,u==U)'; wf(uNdx,:)]);
    tocentroid = tocentroid(1:numUnits);
    S(u==U,1) = sqrt(mean(tocentroid.^2));
end

M = squareform(pdist(A'));

for u = U
    R(:,u==U) = M(:,u==U)./(S(u==U) + S);
end

R(R==0) = nan;
Rmin = min(R);

%% Plot
if toplot
    [~, SCORE] = pca(wf);
    
    hwf = figure('windowstyle','docked');
    plot(mean(wf(ulabel==0,:)),'color',[.5 .5 .5],'linewidth',2)
    hpc = figure('windowstyle','docked');
    plot3(SCORE(ulabel==0,1),SCORE(ulabel==0,2),SCORE(ulabel==0,3),'.','color',[.5 .5 .5])
    
    for u = U(2:end)
        figure(hwf)
        hold on
        plot(mean(wf(ulabel==u,:)),'color',circshift([1 0 0]',u),'linewidth',2)
        figure(hpc)
        hold on
        plot3(SCORE(ulabel==u,1),SCORE(ulabel==u,2),SCORE(ulabel==u,3),'.','color',circshift([1 0 0]',u))
    end
    
end
end

