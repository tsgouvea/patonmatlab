function [ counts, raster, specs, zparam, channel, unit ] = countspk( comb02, bin, nsteps, bounds, evint )
%COUNTSPK Counts spikes from a Combined02 file
%
%   [ counts, raster, specs, zparam ] = countspk( comb02, bin, step, bounds, evint )
%
%   comb02 = output of function mergebn
%
%   bin = Width of bin used to compute spike counts
%
%   step = Number of steps per bin. Set step = 1 for non-overlapping
%   bins.
%
%   bounds = Vector with 2 elements specifying time window during which
%   spikes will be binned and counted. Time window, in milliseconds
%   relative to event of interest (evint), affect the range taken by the
%   upper bound of the bins. If rem(diff(window),(bin/step)) ~= 0, the
%   upper limit of the window is pushed to meet this criterion.
%
%   evint = Event of interest around which spike trains will be aligned.
%   Possible values are 'stimon' (stimulus onset [default]), 'stimoff'
%   (stimulus offset), 'random' (sampled from uniform distribution bounded
%   by trial onset and offset) and 'choice' (first nose-poke at choice port
%   after stimulus onset; includes prematures). Latest additions: 'reward'
%   (valve opening) and 'laser' (first pulse detected in the trial)
%
%   OUTPUT:
%
%   counts = Matrix of spike counts with size t-by-b-by-u, where t is
%   number of trials, b is number of time bins, and u is number of cells.
%
%   specs = Struct with values of input arguments (i.e. bin, step, bounds
%   and evint) used to generate counts and raster.
%
%	zparam = Mean and standard deviation of spike counts per bin, computed
%	across the whole session.
%

%% Inputs
narginchk(1,5);
if nargin < 5
    evint = 'stimon';
    if nargin < 4
        bounds = [-1 3]*1000;
        if nargin < 3
            nsteps = 10;
            if nargin < 2
                bin = 200;
            end
        end
    end
end

if strcmpi(evint,'stimon')
    alignon = comb02.bhv.AlignOn.StimOn;
elseif strcmpi(evint,'stimoff')
    alignon = comb02.bhv.AlignOn.StimOff;
elseif strcmpi(evint,'choice')
    alignon = comb02.bhv.AlignOn.Choice;
elseif strcmpi(evint,'reward')
    alignon = comb02.bhv.AlignOn.Reward;
elseif strcmpi(evint,'laser')
    alignon = comb02.bhv.AlignOn.Laser;
elseif strcmpi(evint,'random')
    alignon = comb02.bhv.AlignOn.Random;
end

%% Adjusting window size
stepsize = bin/nsteps;
bounds = bounds(:);
while rem(diff(bounds),stepsize) ~= 0
    bounds(2) = bounds(2)-rem(diff(bounds),stepsize)+stepsize;
end
nBins = diff(bounds)/stepsize + 1;

%% Outputs
specs.bin = bin; specs.nsteps = nsteps; specs.bounds = bounds; specs.evint = evint;

%% Action!

nTrials = length(comb02.bhv.TrialNumber);
nUnits = length(comb02.neur.unit);

counts = nan(nTrials,nBins,nUnits);
raster = cell(nTrials,nUnits);
zparam = struct('mean',[],'std',[]);
channel = nan(nUnits,1);
unit = nan(nUnits,1);

M = zeros(diff(bounds)+bin+1,nBins);
kernel = ones(1,bin);
% kernel = fliplr(exppdf(1:bin,bin/2));
% kernel = kernel/sum(kernel)%;*1000;%converts ms to s, so that firing rate is in Hz
ndx = [kernel'; zeros(size(M,1)-length(kernel)+stepsize,1)];
ndx2 = repmat(ndx,nBins,1);
ndx3 = ndx2(1:end-(size(M,1)-length(kernel)+stepsize-1));
% M(ndx3) = 1;
M = reshape(ndx3,size(M));

for u = 1:nUnits
    bigTrain = comb02.neur.unit(u).spkTrain;
    mytrials = find(~isnan(comb02.neur.sync) & ~isnan(alignon'))';
%     fineCount = nan(nTrials,nbins+nsteps);
    fineCount = nan(nTrials,diff(bounds)+bin+1);    
    for t = mytrials
        edges = comb02.neur.sync(t) + alignon(t) + (bounds(1)-bin  : bounds(2));
        fineCount(t,:) = histc(bigTrain,edges);
        raster{t,u} = bigTrain(bigTrain>=edges(1)&bigTrain<=edges(end)) - (comb02.neur.sync(t) + alignon(t));
    end
%     plot(bounds(1):stepsize:bounds(2),double(rand(1,4501)<.1)*M)
%     imagesc(double(rand(100,4501)<.1)*M), colorbar
%     plot(bounds(1):stepsize:bounds(2),mean(double(rand(100,4501)<.1)*M))
    counts(:,:,u) = fineCount*M;
    
    sessionCount = histc(bigTrain,min(bigTrain) : bin : max(bigTrain));
    zparam(u).mean = mean(sessionCount);
    zparam(u).std = std(sessionCount);
    channel(u) = double(comb02.neur.unit(u).channel);
    unit(u) = double(comb02.neur.unit(u).unit);
end
end

