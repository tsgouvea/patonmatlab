function [genmo] = thatgm(Xtr3,visL)
%THAT Naive Bayes-estimate of elapsed time from spike counts across a neural population.
%   [genmo] = thatgm(training set), where dimensions of training set
%   are spike count by time bin by neuron.

narginchk(1,2)

if nargin == 1
    visL = false;
end

edges = 0:50; % Range of allowed spike counts
genmo = nan(length(edges),size(Xtr3,2),size(Xtr3,3)); % [firing rate, time, neurons]

sd = 2;
kernel = normpdf(-1*sd*3:1*sd*3,0,sd);
kernel = kernel/sum(kernel);

for u = 1:size(Xtr3,3)
    temp = histc(squeeze(Xtr3(:,:,u)),edges);
    for c = 1:size(temp,2)
%         temp(:,c) = conv(temp(:,c),kernel,'same');
        temp(:,c) = smooth(temp(:,c),30,'lowess')'; % 10 before, Joe uses 30
        temp(temp(:,c)<0,c) = 0; %% NEW - 17 MAY 2014
        temp(:,c) = temp(:,c)/sum(temp(:,c));
    end
    genmo(:,:,u) = temp;    
    clear temp    
end
genmo(genmo<0) = 0;
genmo = .9*genmo + .1*ones(size(genmo))/length(edges);

if visL
    figure
    U = size(Xtr3,3);
    for u = 1:U
        subplot(ceil(sqrt(U)),ceil(sqrt(U)),u),
        imagesc(genmo(:,:,u))
        axis xy
        set(gca,'xtick',[],'ytick',[],'ylim',[1 sum((sum(genmo(:,:,u),2))>.4)])
    end
end

end