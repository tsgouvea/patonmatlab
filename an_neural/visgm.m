function [hgm] = visgm( gm )
%VISGM Displays rich PSTH of every cell in the generative model gm
%   Detailed explanation goes here
Nu = size(gm,3);
panelsize = ceil(sqrt(Nu));
%%
hgm = figure('windowstyle','docked');
for u = 1:Nu
    subplot(panelsize,panelsize,u)
    x = squeeze(gm(:,:,u));
% 	x = x./repmat(sum(x,2),1,size(x,2));
% 	x = x./repmat(sum(x,1),size(x,1),1);
    imagesc(x);
    axis xy
%	set(gca,'xtick',[],'ytick',[],'ylim',[1 sum(nansum(gm(:,:,u),2)>.05)])
end
end

