function [ comb02 ] = mergebn( NEV, dataBhv )
%MERGEBN Merges behavioral and neural data into a combined02.mat file
%
%   NEV = Struct variable produced by Blackrock's openNEV function from a
%   .nev file
%
%   dataBhv = Struct variable produced  by Paton Lab's parseBHV function
%   from .txt file with information from the behavioral box in the format
%   used in the Paton Lab.

narginchk(2,2)

%% Action

%% Adjusting number of trials
syncE = double(NEV.Data.SerialDigitalIO.TimeStamp)'/(NEV.MetaTags.TimeRes/1000); % In ms (for data acquired at 30kHz)
syncB = dataBhv.SyncPulse';
if numel(syncE)>numel(syncB)
    dif = numel(syncE)-numel(syncB);
    df = dif+1; % degrees of freedom
    for d = 1:df
        new_syncE = syncE(d:end-(df-d));
        if numel(new_syncE)==numel(syncB) && corr(diff(new_syncE),diff(syncB)) > 0.99
            syncE = new_syncE;
            break
        end
    end
elseif numel(syncE)<numel(syncB)
    dif = numel(syncB)-numel(syncE);
    df = dif+1;
    for d = 1:df
        %new_syncB = syncB(d:end-(df-d));
        new_syncE = nan(size(syncB));
        new_syncE(d:end-(df-d)) = syncE;
        nanNdx = isnan(new_syncE);
        if numel(new_syncE)==numel(syncB) && corr(diff(new_syncE(~nanNdx)),diff(syncB(~nanNdx))) > 0.99
            syncE = new_syncE;
            break
        end
    end
elseif corr(diff(syncE),diff(syncB)) < .99
    mismatch = true;
    D = 0;
    while mismatch
%         display(['Iter ' sprintf('%3d',D)])
        D = D+1;
        
        new_syncE = nan(size(syncE));
        new_syncE(1:end-D) = syncE(1+D:end);
        nanNdx = isnan(new_syncE);
        if numel(new_syncE)==numel(syncB) && corr(diff(new_syncE(~nanNdx)),diff(syncB(~nanNdx))) > 0.99
            syncE = new_syncE;
            mismatch = false;
            break
        end
        
        new_syncE = nan(size(syncE));
        new_syncE(1+D:end) = syncE(1:end-D);
        nanNdx = isnan(new_syncE);
        if numel(new_syncE)==numel(syncB) && corr(diff(new_syncE(~nanNdx)),diff(syncB(~nanNdx))) > 0.99
            syncE = new_syncE;
            mismatch = false;
            break
        end
        if D > numel(syncB)
            error('PatonLab:neur','Failed to align trials. Are you sure both files refer to the same session?')
        end
    end
end

if strcmp(dataBhv.Animal,'Fernando') && strcmp(dataBhv.Date,'130509')
    syncE([40 184]) = [];
end
nanNdx = isnan(syncE);
if numel(syncE)~=numel(syncB) || corr(diff(syncE(~nanNdx)),diff(syncB(~nanNdx))) < .99
    error('PatonLab:neur','Trials misaligned.')
end

spks = NEV.Data.Spikes;
elctrSet = unique(spks.Electrode);

%% Action!
comb02.bhv = dataBhv;
comb02.neur.sync = syncE;
comb02.neur.unit = struct('spkTrain',[],'sortQual',[]);
U = 0;
for e = elctrSet
    unitSet = unique(spks.Unit(spks.Electrode==e & spks.Unit>0 & spks.Unit<255));
    if ~isempty(unitSet)
        %% Waveforms: PCA and distance matrix.
        wf = double(NEV.Data.Spikes.Waveform(:,spks.Electrode==e & spks.Unit<255))';
        ulabel = double(spks.Unit(spks.Electrode==e & spks.Unit<255))';
        [~, score] = pca(wf);
        
        %% Sort quality index
%         [ R, Rmin ] = davies(wf,ulabel,true);
        try
            nb = NaiveBayes.fit(score(:,1:3),ulabel);
            ulabelHat = predict(nb,score(:,1:3));
        catch
            ulabelHat = nan(size(wf,1),1);
        end
        %%
        for u = double(unitSet)
            U = U+1;
            uNdx = spks.Electrode==e & spks.Unit==u;            
            comb02.neur.unit(U,1).spkTrain = double(spks.TimeStamp(uNdx))/(NEV.MetaTags.TimeRes/1000); % This unit's spikes across the whole session
%             comb02.neur.unit(U,1).sortQual = 'Hoje nao tem. Passa amanha.'; %sortqual Index for this unit
            comb02.neur.unit(U,1).sortQual = 1-pdist([ulabel(ulabel==u)';ulabelHat(ulabel==u)'],'hamming');
            comb02.neur.unit(U,1).waveForm.mean = mean(double(NEV.Data.Spikes.Waveform(:,spks.Electrode==e & spks.Unit==u))');
            comb02.neur.unit(U,1).waveForm.std = std(double(NEV.Data.Spikes.Waveform(:,spks.Electrode==e & spks.Unit==u))');
            comb02.neur.unit(U,1).waveFormNoise.mean = mean(double(NEV.Data.Spikes.Waveform(:,spks.Electrode==e & spks.Unit==0))');
            comb02.neur.unit(U,1).waveFormNoise.std = std(double(NEV.Data.Spikes.Waveform(:,spks.Electrode==e & spks.Unit==0))');
            comb02.neur.unit(U,1).channel = e;
            comb02.neur.unit(U,1).unit = u;
        end        
    end
end
comb02.neur.unit = orderfields(comb02.neur.unit);
%% Synth
% for u = 1:10
%     trialdur = round((90+9*randn)*60*1000);
%     neur(u).spkTrain = sort(randi(trialdur,1,randi(round(trialdur/20))));
%     neur(u).sync = sort(randi(3600000,1,max(dataBhv.TrialNumber)));
%     neur(u).sortqual = tanh(rand*4);
% end

%% End
end

