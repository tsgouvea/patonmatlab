function [ LikeSmooth ] = smoothlike( Like )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

LikeSmooth = Like;
for iNeuron = 1:size(LikeSmooth,3)
%     display([sprintf('%3.0i',iNeuron) '/' sprintf('%3.0i',size(LikeSmooth,3))])
    for iTimebin = 1:size(LikeSmooth,2)
        LikeSmooth(:,iTimebin,iNeuron) = smooth(LikeSmooth(:,iTimebin,iNeuron),10,'lowess')'; % 10 before, Joe uses 30
        LikeSmooth(LikeSmooth(:,iTimebin,iNeuron)<0,iTimebin,iNeuron) = 0;
        LikeSmooth(:,iTimebin,iNeuron) = LikeSmooth(:,iTimebin,iNeuron)/sum(LikeSmooth(:,iTimebin,iNeuron));
    end
end
while any(LikeSmooth(:)==0)
    LikeSmooth = .99*LikeSmooth + .01*ones(size(LikeSmooth))/size(LikeSmooth,1);
end
end

